package com.ingress.ms1.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingress.ms1.Entities.ApiCounterEntity;

public interface ApiCounterRepo extends JpaRepository<ApiCounterEntity,Long> {
    
}
