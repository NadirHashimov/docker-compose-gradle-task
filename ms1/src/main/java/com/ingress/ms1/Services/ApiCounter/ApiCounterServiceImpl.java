package com.ingress.ms1.Services.ApiCounter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ingress.ms1.Entities.ApiCounterEntity;
import com.ingress.ms1.Repositories.ApiCounterRepo;

@Service
public class ApiCounterServiceImpl implements ApiCounterService {

    @Autowired
    private ApiCounterRepo apiCounterRepository;

    @Override
    @Transactional
    public Long incrementAndGetCount() {
        ApiCounterEntity apiCounter = apiCounterRepository.findById(1L).orElse(new ApiCounterEntity());
        apiCounter.setCount(apiCounter.getCount() + 1);
        apiCounterRepository.save(apiCounter);
        return apiCounter.getCount();
    }
    
}
