package com.ingress.ms1.Services.ApiCounter;

public interface ApiCounterService {
    public Long incrementAndGetCount();
}
