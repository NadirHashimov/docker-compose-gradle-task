package com.ingress.ms2.Services.ApiCounter;

public interface ApiCounterService {
    public Long incrementAndGetCount();
}
