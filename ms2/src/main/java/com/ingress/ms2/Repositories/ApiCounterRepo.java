package com.ingress.ms2.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ingress.ms2.Entities.ApiCounterEntity;

public interface ApiCounterRepo extends JpaRepository<ApiCounterEntity,Long> {
    
}
