package com.ingress.ms2.APIs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ingress.ms2.Services.ApiCounter.ApiCounterService;

@RestController
@RequestMapping("/api/v1")
public class HelloController {
    @Autowired
    private ApiCounterService apiCounterService;

    @GetMapping("/hello")
    public String getCallCount() {
        return "Hello from ms2, count:" + apiCounterService.incrementAndGetCount();
    }
}
